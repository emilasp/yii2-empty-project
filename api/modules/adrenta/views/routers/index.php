<?php

use emilasp\core\components\gridview\RelationColumn;
use emilasp\media\components\gridview\ImageColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\companies\models\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('adrenta', 'Routers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index box box-primary">

    <div class="box-body table-responsive no-padding">

        <div class="box-header with-border">
            <?= Html::a(Yii::t('site', 'Create'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </div>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel'  => $searchModel,
            'layout'       => "{items}\n{summary}\n{pager}",
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

               'name',
                ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
