<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'             => 'rest-api-frontend',
    'basePath'       => dirname(__DIR__),
    'bootstrap'      => ['log'],
    'language'       => 'ru_RU',
    'sourceLanguage' => 'en_US',

    'modules' => [
        'users'  => [
            'class' => 'emilasp\users\api\UsersModule',
        ],
        'course' => [
            'class' => 'emilasp\course\api\CourseModule',
        ],
        'social' => [
            'class' => 'emilasp\social\api\SocialModule',
        ],
    ],

    'components' => [
        'user'       => [
            'class'         => \emilasp\users\common\components\UserComponent::className(),
            //'accessChecker'   => '\emilasp\rights\components\RightsGroupCheckAccess',
            'identityClass' => '\emilasp\users\common\models\User',
            'enableSession' => false,
            //'loginUrl'        => '/users/user/login.html'
        ],
        'urlManager' => [

            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'POST users/auth/login'            => 'users/auth/login',
                'POST users/registration/index'    => 'users/registration/index',
                'POST users/manage/create'         => 'users/manage/create',

                'GET users/profile/user-info'      => 'users/profile/user-info',
                'GET users/profile/referrals'      => 'users/profile/referrals',
                'POST users/profile/avatar'        => 'users/profile/avatar',
                'POST users/profile/invite'        => 'users/profile/invite',

                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => ['users/manage'],
                    'except'     => ['delete'],
                    //'pluralize'  => false
                ],
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => ['course/course-user-link'],
                    'except'     => ['delete'],
                    /* 'tokens'        => [
                         '{code}' => '<code:\\w+>',
                     ],
                     'extraPatterns' => [
                         'GET {code}' => 'view',
                     ],*/
                    'pluralize'  => false
                ],
                'GET course/course-user-link/view' => 'course/course-user-link/view',
                'GET course/lesson-user-link/view' => 'course/course-lesson-user-link/view',
                'GET social/comment/list' => 'social/comment/list',
                'POST social/comment/create' => 'social/comment/create',

            ],
            //    ['class' => 'yii\rest\UrlRule', 'controller' => 'users/manage',],
            //    /* [
            //         'class' => 'yii\rest\UrlRule',
            //         'controller' => 'v1/service',
            //         'patterns' => [
            //             'GET {msisdn}' => 'index',
            //             'POST' => 'enable',
            //             'DELETE' => 'disable',
            //         ],
            //         'tokens' => [
            //             '{msisdn}' => '<msisdn:\\d[\\d,]*>',
            //         ]
            //     ],*/
            //]
        ],
        'request'    => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'text/plain'       => 'yii\web\JsonParser',
            ]
        ],
        'response'   => [
            'class'  => 'yii\web\Response',
            'format' => yii\web\Response::FORMAT_JSON,
        ],
    ],


    /* 'as beforeRequest' => [
         'class' => 'emilasp\seo\behaviors\SeoMetaFilter',
     ],*/

    'params' => $params,
];
