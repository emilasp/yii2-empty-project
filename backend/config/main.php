<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules'             => [
        'core' => [
            'class'             => '\emilasp\core\CoreModule',
            'enableTheme'       => true,
            'enableModuleTheme' => true,
            'theme'             => 'default',
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],

        'issue' => [
            'class' => 'emilasp\userissue\UsersIssueModule',
        ],

        'settings' => [
            'class'       => 'emilasp\settings\SettingModule',
            'onAdminPage' => [
                \emilasp\users\backend\UsersModule::className(),
            ],
        ],

        'users' => [
            'class' => 'emilasp\users\backend\UsersModule',
            'routeAfterLogin' => '/adrenta/routers/index'
        ],

        'rights' => [
            'class' => 'emilasp\rights\RightsModule',
        ],

        'site'   => [
            'class' => 'emilasp\site\backend\SiteModule',
        ],


        'admin' => [
            'class' => 'backend\modules\admin\AdminModule',
        ],

        'course'   => [
            'class' => 'emilasp\course\backend\CourseModule',
        ],

        'social' => [
            'class' => 'emilasp\social\backend\SocialModule',
        ],
    ],
    'components'          => [
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation'   => true,
            'cookieValidationKey'    => 'gdfgg345tfg<as5,fd890sdasdg786hrHBKMJFW9EG',
        ],
        'user'    => [
            'class'           => \emilasp\users\common\components\UserComponent::className(),
            'accessChecker'   => '\emilasp\rights\components\RightsGroupCheckAccess',
            'identityClass'   => '\emilasp\users\common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl'        => '/users/user/login.html'
        ],

        'session' => [
            'class'        => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 3600 * 24 * 30 * 12],
            'timeout'      => 3600 * 24 * 30 * 12,
            'useCookies'   => true,
        ],

        /* 'user'    => [
             'class'           => 'emilasp\users\common\components\UserComponent',
             'identityClass'   => 'emilasp\users\common\models\User',
             'enableAutoLogin' => true,
             'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
             'loginUrl'        => ['/users/user/login']
         ],*/
        'access'  => [
            'class' => 'yii\web\AccessControl',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => '/site/site/error',
        ],


        'assetManager' => [
            'linkAssets' => YII_DEBUG,

            /* 'bundles' => [
                 'dmstr\web\AdminLteAsset' => [
                     'skin' => 'skin-black',
                 ],
             ],*/
        ],

        'view' => [
            'class' => 'emilasp\core\components\base\ViewWithoutMinify',
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/default/views',
                    //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],
    ],
    'params'              => $params,
];
