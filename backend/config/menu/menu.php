<?php

use emilasp\cms\common\models\ContentCategory;
use yii\helpers\Url;

$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [

    [
        'label' => 'Курсы',
        'url'   => '#',
        'icon'  => 'fa fa-bars',
        'items' => [
            [
                'label' => 'Курсы',
                'url'   => '/course/course/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'course/course/index'],
            ],
            [
                'label' => 'Блоки',
                'url'   => '/course/block/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'course/block/index'],
            ],
            [
                'label' => 'Уроки',
                'url'   => '/course/lesson/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'course/lesson/index'],
            ],
            [
                'label' => 'Курс-пользователь',
                'url'   => '/course/course-user-link/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'course/course-user-link/index'],
            ],
            [
                'label' => 'Урок-пользователь',
                'url'   => '/course/lesson-user-link/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'course/lesson-user-link/index'],
            ],
        ],
    ],
    [
        'label' => 'Роутеры',
        'url'   => '#',
        'icon'  => 'fa fa-wifi',
        'items' => [
            [
                'label' => 'Список',
                'url'   => '/adrenta/routers/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'adrenta/routers/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('users', 'Users'),
        'url'   => '#',
        'icon'  => 'fa fa-users',
        'items' => [
            [
                'label' => Yii::t('users', 'Management'),
                'url'   => '/users/manage/index',
                'icon'  => 'fa fa-user',
                'role'  => ['admin', 'users/manage/index'],
            ],
            [
                'label' => Yii::t('users', 'Issues'),
                'url'   => '/issue/user-issue/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'users/user/index'],
            ],
        ],
    ],

    [
        'label' => Yii::t('rights', 'Rights'),
        'url'   => '#',
        'icon'  => 'fa fa-lock',
        'items' => [
            [
                'label' => Yii::t('rights', 'Groups'),
                'url'   => '/rights/rights-group/index',
                'icon'  => 'fa fa-check-square',
                'role'  => ['admin', 'rights/rights-group/index'],
            ],
            [
                'label' => Yii::t('rights', 'Actions'),
                'url'   => '/rights/rights-action/index',
                'icon'  => 'fa fa-anchor',
                'role'  => ['admin', 'rights/rights-action/index'],
            ],
            [
                'label' => Yii::t('rights', 'Parse Actions'),
                'url'   => '/rights/parse-actions/index',
                'icon'  => 'fa fa-bars',
                'role'  => ['admin', 'rights/parse-actions/index'],
            ],
        ],
    ],

    [
        'label'  => Yii::t('social', 'Social'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'icon'   => 'fa fa-list-alt',
        'items'  => [
            [
                'label'  => Yii::t('social', 'Comments'),
                'url'    => '/social/comment/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('social', 'Rates'),
                'url'    => '/social/rating/index',
                'active' => false,
                'role'   => '@',
            ],
        ]
    ],

    [
        'label' => Yii::t('media', 'Files'),
        'url'   => '#',
        'icon'  => 'fa fa-folder',
        'role'  => ['admin'],
        'items' => [
            ['label' => 'Список', 'url' => '/files/file/index', 'role' => ['@']],
            ['label' => 'Добавить', 'url' => '/files/file/create', 'role' => ['@']],
        ],
    ],


    [
        'label' => Yii::t('settings', 'Settings'),
        'url'   => '/settings/default/index',
        'icon'  => 'fa fa-cogs',
        'role'  => ['admin'],
        //'visible' => Yii::$app->user->can('admin')
    ],

    [
        'label' => 'SYSTEM',
        'url'   => '#',
        'icon'  => 'fa fa-users',
        'items' => [
            [
                'label' => 'Logger Logs',
                'url'   => '/system/logger/index',
                'icon'  => 'fa fa-ellipsis-h',
                'role'  => ['admin'],
            ],
            [
                'label' => 'Правка начальных балансов',
                'url'   => '/system/accounting/update-balance-start',
                'icon'  => 'fa fa-ellipsis-h',
                'role'  => ['admin'],
            ],


        ],
    ],


    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '/users/auth/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label'       => Yii::t('users', 'Logout') . ' <small>(' . $username . ')</small>',
        'url'         => '/users/user/logout',
        'icon'        => 'fa fa-key',
        'linkOptions' => ['data-method' => 'POST'],
        'role'        => ['@'],
        'visible'     => !Yii::$app->user->isGuest
    ],
];
