<?php
$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [
    [
        'label'  => 'Статистика(Old)',
        'url'    => '/punishment/user-head/sro',
        'icon'   => 'fa fa-list-alt',
        'active' => false,
        'role'   => ['punishment-head'],
    ],
];
