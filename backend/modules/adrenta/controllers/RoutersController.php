<?php

namespace backend\modules\adrenta\controllers;

use Yii;
use backend\modules\faqs\models\Faq;
use emilasp\core\components\base\Controller;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * RoutersController implements the CRUD actions for Faq model.
 */
class RoutersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new FaqSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $routers = [
            ['name' => 'Роутер 1'],
            ['name' => 'Роутер 2'],
            ['name' => 'Роутер 3'],
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels'  => $routers,
            'sort'       => [
                'attributes' => [
                    'company_name',
                    'name',
                    'balance',
                    'voice_send_at',
                    'voice_segment_name',
                    'operator_call_at',
                    'paid_sum',
                    'paid_at',
                    'email_at',
                    'email_is_read',
                ],
            ],
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Faq::className()),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, Faq::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Faq::className())->delete();

        return $this->redirect(['index']);
    }
}
