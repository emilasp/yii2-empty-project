<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\telephony\models\Call */

$this->title                   = 'Загрузка сегментов';
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="call-view box box-primary">
        <div class="box-header">

            <div class="row">
                <div class="col-md-6">
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                    <?= $form->field($model, 'token')->textInput(['class' => 'form-control']) ?>

                    <?= $form->field($model, 'name')->textInput(['class' => 'form-control']) ?>

                    <?= $form->field($model, 'file')->fileInput(['class' => 'form-control']) ?>

                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>

                    <?php ActiveForm::end() ?>
                </div>
                <div class="col-md-6 text-right">
                    <div class="box-logs">
                        <?php if ($model->response): ?>
                            <?php foreach ($model->response as $response) : ?>
                                <?= VarDumper::dumpAsString($response, 100, true) ?>
                            <?php endforeach; ?>
                        <?php endIf; ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="box-body table-responsive no-padding">

        </div>
    </div>

<?php
$urlSendFile = Url::toRoute(['/admin/auditory/upload']);
/*
$js = <<<JS
    $(document).on('click', '.btn-account-send', function() {
        var button = $(this);
        var email = $('.account-send-email').val();
        
        button.button('loading');
        
        $.ajax({
            type: 'POST',
            url: '{$urlSendFile}',
            dataType: "json",
            data: {id:, email:email},
            success: function(msg) {
                if(msg.status == 1){
                   notice(msg.message, "green");
                }else{
                   notice(msg.message, "red");
                }
                button.button('reset');
            }
    });
    });
JS;

$this->registerJs($js);*/