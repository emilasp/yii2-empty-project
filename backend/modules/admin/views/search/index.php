<?php

use backend\modules\admin\widgets\AdminSearchForm\AdminSearchFormWidget;
use backend\modules\companies\models\Company;
use backend\modules\counteragents\models\Counteragent;
use emilasp\core\components\gridview\RelationColumn;
use emilasp\core\helpers\StringHelper;
use emilasp\media\components\gridview\ImageColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title                   = 'Поиск';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="search-view box box-primary">
    <div class="box-header">

        <?= AdminSearchFormWidget::widget() ?>

        <?php if ($companyProvider->count) : ?>
            <h2>Компании</h2>

            <?php Pjax::begin(); ?>

            <?= GridView::widget([
                'dataProvider' => $companyProvider,
                'layout'       => "{items}\n{summary}\n{pager}",
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'file',
                        'class'     => ImageColumn::className(),
                    ],
                    [
                        'attribute' => 'name_short',
                        'class'     => RelationColumn::className(),
                        'name'      => 'name',
                        'route'     => '/companies/company/view',
                    ],
                    'inn',
                    [
                        'attribute' => 'status',
                        'label'     => 'Долг',
                        'value'     => function ($model, $key, $index, $column) {
                            $sum   = $model->getBalance(true);
                            $class = $sum > 0 ? 'text-danger' : 'text-success';
                            return Html::tag('span', Yii::$app->formatter->asCurrency($sum), ['class' => $class]);
                        },
                        'class'     => '\yii\grid\DataColumn',
                        'format'    => 'raw',
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        <?php endif; ?>

        <?php if ($counteragentProvider->count) : ?>
            <h2>Контрагенты</h2>

            <?php Pjax::begin(); ?>

            <?= GridView::widget([
                'dataProvider' => $counteragentProvider,
                'layout'       => "{items}\n{summary}\n{pager}",
                'columns'      => [
                    [
                        'attribute' => 'company.file',
                        'class'     => ImageColumn::className(),
                    ],
                    [
                        'attribute' => 'name_short',
                        'class'     => RelationColumn::className(),
                        'name'      => 'name_short',
                        'route'     => '/counteragents/counteragent/view',
                    ],

                    'inn',

                    [
                        'attribute' => 'company_id',
                        'value'     => function ($model, $key, $index, $column) {
                            return $model->company->name_short ?? null;
                        },
                        'class'     => '\yii\grid\DataColumn',
                        'filter'    => Company::find()->map('id', 'name_short')->all(),
                    ],

                    'city',

                    [
                        'attribute' => 'status',
                        'value'     => function ($model, $key, $index, $column) {
                            $status = Yii::t('counteragents', Counteragent::$statuses[$model->status]);
                            $class  = $model->status === Counteragent::STATUS_MEMBER ? 'text-success' : 'text-danger';

                            return Html::tag('span', $status, ['class' => $class]);
                        },
                        'class'     => '\yii\grid\DataColumn',
                        'filter'    => StringHelper::yiiTranslateArray('counteragents', Counteragent::$statuses),
                        'format'    => 'raw',
                    ],

                    [
                        'attribute' => 'balance',
                        'label'     => 'Баланс',
                        'value'     => function ($model, $key, $index, $column) {
                            $sum   = $model->balance;
                            $class = $sum < 0 ? 'text-danger' : 'text-success';
                            return Html::tag('span', Yii::$app->formatter->asCurrency($sum), ['class' => $class]);
                        },
                        'class'     => '\yii\grid\DataColumn',
                        'format'    => 'raw',
                    ],
                ]
            ]); ?>

            <?php Pjax::end(); ?>

        <?php endif; ?>

    </div>
    <div class="box-body table-responsive no-padding">

    </div>
</div>
