<?php

namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\httpclient\Client;

/**
 * Login form
 */
class AuditoryModel extends Model
{
    const PATH_SAVE_FILE = '@console/runtime/data/tmp.log';
    public $file;
    public $token = 'AQAEA7qh7wn0AAS8CL57VOvoykPgsQlP-KIOcUE';
    public $name;

    public $response = [];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token'], 'required'],
            [['token', 'name'], 'string', 'max' => 100],
            [['file'], 'file'],
        ];
    }

    /**
     * Сохраняем файл
     *
     * @return bool
     */
    public function saveFile(): bool
    {
        $filePath = Yii::getAlias(self::PATH_SAVE_FILE);

        if (is_file($filePath)) {
            unlink($filePath);
        }

        $this->file->saveAs($filePath);
        return true;
    }

    /**
     * Create segment
     */
    public function createSegment(): array
    {
        $response = $this->sendRequest(
            'https://api-audience.yandex.ru/v1/management/segments/upload_file',
            [
                'segment' => [
                    'name'         => $this->name,
                    'content_type' => 'mac  ',
                ]
            ],
            'POST',
            self::PATH_SAVE_FILE
        );

        return json_decode($response, true);
    }

    /**
     * Save segment
     */
    public function saveSegment(string $segmentId, string $name): array
    {
        $url = "https://api-audience.yandex.ru/v1/management/segment/{$segmentId}/confirm";

        $name .= date('_dmY');

        $response = $this->sendRequest(
            $url,
            [
                'segment' => [
                    'id'           => $segmentId,
                    'name'         => $name,
                    'hashed'       => 0,
                    'content_type' => 'mac',
                ]
            ],
            'POST'
        );

        return json_decode($response, true);
    }

    private function sendRequest(string $url, array $data = [], string $method = 'GET', string $filePath = ''): string
    {
        $client  = new Client();
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($url)
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders([
                'Authorization' => 'OAuth ' . $this->token
            ])
            ->setData($data);

        if ($filePath) {
            $request->addFile('file', Yii::getAlias($filePath));
        }

        $response = $request->send();

        $this->response[] = json_decode($response->content, true);

        if ($response->isOk) {
            return $response->content;
        }

        return $response->content;
    }
}
