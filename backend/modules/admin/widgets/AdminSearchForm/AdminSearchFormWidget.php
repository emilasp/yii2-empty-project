<?php

namespace backend\modules\admin\widgets\AdminSearchForm;

use emilasp\core\components\base\Widget;
use Yii;

/**
 * Class AdminSearchFormWidget
 * @package backend\modules\users\widgets\AdminSearchForm
 */
class AdminSearchFormWidget extends Widget
{
    public $route = '/counteragents/counteragent/index';

    /**
     * RUN
     */
    public function run(): void
    {
        echo $this->render('form', [
            'search' => Yii::$app->request->get('search'),
        ]);
    }
}
