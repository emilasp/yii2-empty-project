<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

    <div>
        <?= Html::textInput('admin-search', $search, [
            'class'       => 'form-control admin-search-input',
            'placeholder' => 'поиск'
        ]) ?>
    </div>

<?php

$url = Url::toRoute(['/admin/search/index']);

$js = <<<JS
   $(document).on('change', '.admin-search-input', function () {
       var input = $(this);
       var url = '{$url}';
      
       location.href = url + '?search=' + input.val();
       
   });
JS;

$this->registerJs($js);