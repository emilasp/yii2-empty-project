<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\AuditoryModel;
use backend\modules\emails\components\EmailSenderComponent;
use backend\modules\users\components\userReplace\UserReplaceAction;
use backend\modules\users\components\userReplace\UserReplaceFilter;
use backend\modules\users\models\User;
use emilasp\core\components\base\Controller;
use backend\modules\users\models\forms\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\UploadedFile;

/**
 * Auth controller
 */
class AuditoryController extends Controller
{
    private const FILE_PATH = '@console/runtime/data/tmp.log';

    private $token;
    private $name;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'grunt'],
                'rules' => [
                    [
                        'actions' => ['index', 'interface', 'grunt'],
                        'allow'   => true,
                        'roles'   => ['?', '@'],
                    ],
                ],
            ],
            /* 'verbs'       => [
                 'class'   => VerbFilter::className(),
                 'actions' => [
                     'logout' => ['post'],
                 ],
             ],*/
        ];
    }

    //pv spb02_mmops_scaner_1.log | awk '{print $7}' | sed -e s/^SA:// | sort | uniq
    //pv Загрузки/std.txt  |  sort | tr '[:lower:]' '[:upper:]' |  sed 's/:s*//'  |  sed 's/:s*//' |  sed 's/:s*//' |  sed 's/:s*//' |  sed 's/:s*//' > Загрузки/tmp.log
    //http://bscrm.ru/admin/auditory/index.html?token=AQAEA7qh7wn0AAS8CL57VOvoykPgsQlP-KIOcUE&name=HEX_MAC
    //http://bscrm.ru/admin/auditory/save-segment.html?token=AQAEA7qh7wn0AAS8CL57VOvoykPgsQlP-KIOcUE&segment=SEGMENT_ID&name=HEX_MAC
    //http://bscrm.ru/admin/auditory/grunt.html?token=AQAEA7qh7wn0AAS8CL57VOvoykPgsQlP-KIOcUE&segment=SEGMENT_ID&login=USER_LOGIN
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function _actionIndex(string $token, string $name)
    {
        $this->token = $token;
        $this->name  = $name;

        echo $this->createSegment();


        //return $this->render('index');
    }

    public function actionIndex()
    {
        $model = new AuditoryModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->saveFile()) {
                $response = $model->createSegment();
                $response = $model->saveSegment($response['segment']['id'], $model->name);
            }
        }


        return $this->render('index', ['model' => $model]);
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionGrunt(string $token, string $segment, string $login)
    {
        $this->token = $token;
        echo $this->createGrant($segment, $login);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionSaveSegment(string $token, string $segment, string $name)
    {
        $this->token = $token;

        echo $this->save($segment, $name);
    }

    /**
     *
     */
    private function createSegment()
    {
        return $this->sendRequest(
            'https://api-audience.yandex.ru/v1/management/segments/upload_file',
            [
                'segment' => [
                    'name'         => $this->name,
                    'content_type' => 'mac  ',
                ]
            ],
            'POST',
            self::FILE_PATH
        );
    }

    /**
     *
     */
    private function createGrant(string $segmentId, string $userLogin)
    {
        $url = "https://api-audience.yandex.ru/v1/management/segment/{$segmentId}/grant";

        return $this->sendRequest(
            $url,
            [
                'grant' => [
                    'user_login' => $userLogin,
                    'comment'    => 'mac hex',
                ]
            ],
            'PUT'
        );
    }

    /**
     *
     */
    private function save(string $segmentId, string $name)
    {
        $url = "https://api-audience.yandex.ru/v1/management/segment/{$segmentId}/confirm";

        return $this->sendRequest(
            $url,
            [
                'segment' => [
                    'id' => $segmentId,
                    'name'    => $name,
                    'hashed'    => 0,
                    'content_type'    => 'mac',
                ]
            ],
            'POST'
        );
    }

    private function sendRequest(string $url, array $data = [], string $method = 'GET', string $filePath = ''): string
    {
        $client  = new Client();
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($url)
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders([
                'Authorization' => 'OAuth ' . $this->token
            ])
            ->setData($data);

        if ($filePath) {
            $request->addFile('file', Yii::getAlias($filePath));
        }

        $response = $request->send();
        if ($response->isOk) {
            return $response->content;
        }

        return $response->content;
    }

    /* private function getToken()
    {
        $urlToToken = 'https://oauth.yandex.ru/authorize';

        $data = [
            'response_type' => 'token',
            'client_id'     => '3a793a0072d14aa8bdc8d84123fa06e4',
        ];

        $client   = new Client();
        $response = $client->createRequest()
            ->setHeaders([])
            ->setMethod('GET')
            ->setUrl($urlToToken)
            ->setData($data)
            ->send();
        if ($response->isOk) {
            return $response->content;
        } //https://oauth.yandex.ru/authorize?client_id=3a793a0072d14aa8bdc8d84123fa06e4&response_type=token

        return ['content' => $response->content];
    }*/
}
