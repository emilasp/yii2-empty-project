<?php

namespace backend\modules\admin\controllers;

use backend\modules\companies\models\Company;
use backend\modules\counteragents\models\search\CounteragentSearch;
use Yii;
use emilasp\core\components\base\Controller;
use backend\modules\rights\filters\AccessControl;

/**
 * SearchController
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Обрабатываем запрос на поиск
     * @return mixed
     */
    public function actionIndex(string $search)
    {
        $companyProvider      = (new Company())->searchByKeyword($search, ['name', 'inn']);
        $counteragentProvider = (new CounteragentSearch())->searchByKeyword($search, ['name', 'inn']);

        return $this->render('index', [
            'companyProvider'      => $companyProvider,
            'counteragentProvider' => $counteragentProvider,
        ]);
    }
}
