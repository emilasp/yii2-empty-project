<?php

namespace backend\modules\reports\controllers;

use backend\modules\counteragents\models\search\CounteragentSearch;
use Yii;
use emilasp\core\components\base\Controller;
use backend\modules\rights\filters\AccessControl;
use yii\base\DynamicModel;

/**
 * TestController
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CounteragentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('email', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionEmail()
    {
        $model = new DynamicModel(['name', 'email', 'address']);
        $model
            ->addRule(['name', 'email'], 'required')
            ->addRule(['email'], 'email')
            ->addRule('address', 'string', ['max' => 32]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        }
        return $this->render('email', ['model' => $model]);
    }
}
