<?php

namespace backend\modules\admin;

use emilasp\core\CoreModule;

/**
 * Class AdminModule
 * @package backend\modules\admin
 */
class AdminModule extends CoreModule
{
}
