<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row-centered">
    <div class="col-xs-2 col-centered">

        <div class="row login-form-container">
            <div class="col-md-12">

                <br/>
                <br/>
                <br/>
                <br/>

                <?php Pjax::begin(['id' => 'login-form-pjax']); ?>
                <?php
                $form = ActiveForm::begin([
                    'id'      => 'login-form',
                    'options' => ['class' => 'login-form']
                ]); ?>

                <div class="login-form-block">

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/images/enter.png" alt=""
                                 class="circle responsive-img valign profile-image-login">

                            <h4 class="center login-form-text"><?= Yii::t('users', 'Please login') ?></h4>
                        </div>
                    </div>
                    <div class="">
                        <?= $form->field($model, 'username')->textInput([
                            'autofocus'   => true,
                            'placeholder' => $model->getAttributeLabel('username'),
                        ])->label(false) ?>
                    </div>
                    <div class="">
                        <?= $form->field($model, 'password')->passwordInput([
                            'placeholder' => $model->getAttributeLabel('password'),
                        ])->label(false) ?>
                    </div>

                    <div class="">
                        <?= Html::input('text', 'inpute-invite', null, [
                            'class'       => 'form-control',
                            'placeholder' => 'ИНВАЙТ'
                        ]) ?>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('users', 'Login'), [
                                    'class' => 'btn btn-lg btn-success btn-block'
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'rememberMe')->checkbox(['class' => ''], false) ?>
                        </div>
                    </div>

                </div>

                <?php ActiveForm::end(); ?>

                <?php Pjax::end(); ?>

            </div>
        </div>


    </div>
</div>

