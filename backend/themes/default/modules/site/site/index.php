<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->params['breadcrumbs'][] = $model->name;
?>
<div class="index-page">
    <h1><?= Html::encode($model->name) ?></h1>

    <?= Html::decode($model->text) ?>
</div>