<?php
return [
    'enablePrettyUrl'     => true,
    'enableStrictParsing' => true,
    'showScriptName'      => false,
    'rules'               => [
        ''                                            => '/users/dashboard/index',
        'images/logo_<hash:\w+>.jpeg'                      => '/emails/checker/check',

        '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>.html' => '<_m>/<_c>/<_a>',

        '/users/service/auth/<service>.html' => '/users/service/auth',
        '/sitemap.xml' => '/site/sitemap/index',
        '/media/file/cke-upload' => '/media/file/cke-upload',
    ],
];
