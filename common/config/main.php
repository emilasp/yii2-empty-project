<?php
return [
    'aliases'    => [
        '@bower' => '@vendor/bower',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language'            => 'ru_RU',
    'sourceLanguage'      => 'en_US',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules'    => [
        'variety' => [
            'class' => 'emilasp\variety\VarietyModule',
        ],
        'json'    => [
            'class' => 'emilasp\json\JsonModule',
        ],

        'settings' => [
            'class' => 'emilasp\settings\SettingModule',
        ],

        'media' => [
            'class'     => 'emilasp\media\MediaModule',
            'pathCache' => '@backend/web/media/files',
            'config'    => [
                'watermarkSrc'   => '@backend/web/images/watermark.png',
                'scaleWatermark' => 5,
                'quality'        => 100,
                'noImage'        => [
                    'path' => '/media/system/noImage/',
                    'file' => 'no-image.png',
                ],
            ],
            'sizes'     => [
                'default' => [
                    \emilasp\media\models\File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                    \emilasp\media\models\File::SIZE_MIN => ['size' => '150x120', 'watermark' => true, 'aspectRatio' => true],
                    \emilasp\media\models\File::SIZE_MID => ['size' => '250x200', 'watermark' => true, 'aspectRatio' => true],
                    \emilasp\media\models\File::SIZE_MED => ['size' => '400x360', 'watermark' => true, 'aspectRatio' => true],
                    \emilasp\media\models\File::SIZE_MAX => ['size' => '800x600', 'watermark' => true, 'aspectRatio' => true],
                    \emilasp\media\models\File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                ],
                /*  Profile::className() => [
                      File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                      File::SIZE_MIN => ['size' => '96x144', 'watermark' => true, 'aspectRatio' => true],
                      File::SIZE_MID => ['size' => '218x320', 'watermark' => true, 'aspectRatio' => true],
                      File::SIZE_MED => ['size' => '400x600', 'watermark' => true, 'aspectRatio' => true],
                      File::SIZE_MAX => ['size' => '800x600', 'watermark' => true, 'aspectRatio' => true],
                      File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                  ],*/
            ],
        ],
    ],

    'components' => [
        'db'         => require(__DIR__ . '/include/db.php'),
        'redis'      => require(__DIR__ . '/include/redis.php'),
        //'sphinx' => require(__DIR__ . '/include/sphinx.php'),
        'mailer'     => require(__DIR__ . '/include/mail.php'),
        'i18n'       => require(__DIR__ . '/include/i18n.php'),
        'urlManager' => require(__DIR__ . '/include/urlManager.php'),

        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'redis',
                'port'     => 6379,
                'database' => 2,
            ],
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
        ],
    ],
];
