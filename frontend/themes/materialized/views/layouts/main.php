<?php
/* @var $this \yii\web\View */

/* @var $content string */

use emilasp\core\assets\ThemeBaseAsset;
use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\frontend\widgets\CategoryWidget\CategoryWidget;
use emilasp\cms\frontend\widgets\ContentSidebarWidget\ContentSidebarWidget;
use emilasp\cms\frontend\widgets\TagWidget\TagWidget;
use emilasp\core\extensions\AjaxProgressLine\AjaxProgressLine;
use emilasp\site\common\extensions\MenuCustom\MenuCustom;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\users\frontend\widgets\LoginWidget\LoginWidget;
use yii\helpers\Html;
use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use emilasp\site\common\extensions\breadcrumbs\Breadcrumbs;

?>

<?php ThemeBaseAsset::register($this); ?>
<?php AppAsset::register($this); ?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title itemprop="name"><?= Html::encode(strip_tags($this->title)) ?></title>

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<?= MenuCustom::widget(['type' => 'menu-bs4']) ?>

<div class="container content-wrapper">

    <div class="row">

        <div class="col-md-4 col-lg-3">

            <div class="sidebar-widget content-block">
                <h3><?= Yii::t('taxonomy', 'Categories') ?></h3>

                <?= CategoryWidget::widget(['categoryClass' => ContentCategory::className()]) ?>
            </div>

            <div class="sidebar-widget content-block">
                <h3><?= Yii::t('cms', 'Popular') ?></h3>

                <?= ContentSidebarWidget::widget(['className' => Article::className()]) ?>
            </div>

            <div class="sidebar-widget content-block">
                <h3><?= Yii::t('taxonomy', 'Tags') ?></h3>

                <?= TagWidget::widget() ?>
            </div>

        </div>

        <div class="col-md-8 col-lg-9">

            <?php FlashMsg::widget(); ?>

            <div class="row content-block breadcrumb-container">
                <div class="col-md-12">
                    <?= Breadcrumbs::widget(['items' => $this->params['breadcrumbs']]) ?>

                    <?= AjaxProgressLine::widget() ?>
                </div>
            </div>

            <div class="row content-block">
                <div class="col-md-12">
                    <?= $content ?>
                </div>
            </div>

        </div>

    </div>

</div>


<footer class="footer-black">

    <div class="footer-center menu-style">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <nav class="col-xs-12 col-sm-6">
                            <p class="nav-title">Источникики энергии</p>
                            <ul class="header-footer">
                                <li class="header-footer__item"><a href="/cms/article/category/3/vidy-energii-solnechnaya-energiya-solnechnaya-energiya.html" title="Солнечная энергия">Солнечная энергия</a></li>
                                <li class="header-footer__item"><a href="/cms/article/category/4/eneriya-vetra.html" title="Энерия ветра">Энерия ветра</a></li>
                                <li class="header-footer__item"><a href="/cms/article/category/13/texnologii.html" title="Технологии">Технологии</a></li>
                            </ul>
                        </nav>
                        <nav class="col-xs-12 col-sm-6">
                            <p class="nav-title">Технологии</p>
                            <ul class="header-footer">
                                <li class="header-footer__item"><a href="/cms/article/category/13/texnologii.html" title="Технологии">Технологии</a></li>
                                <li class="header-footer__item"><a href="/cms/article/category/13/texnologii.html" title="Технологии">Технологии</a></li>
                                <li class="header-footer__item"><a href="/cms/article/category/13/texnologii.html" title="Технологии">Технологии</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <nav class="col-xs-12 col-sm-6">
                            <p class="nav-title">Илон Маск</p>
                            <ul class="header-footer">
                                <li class="header-footer__item"><a href="" title="Tesla">Tesla</a></li>
                                <li class="header-footer__item"><a href="" title="SpaceX">SpaceX</a></li>
                            </ul>
                        </nav>
                        <nav class="col-xs-12 col-sm-6">
                            <p class="nav-title">Разное</p>
                            <ul class="header-footer">
                                <li class="header-footer__item"><a href="#" title="Статьи">Статьи</a></li>
                                <li class="header-footer__item"><a href="#" title="Категории">Категории</a></li>
                                <li class="header-footer__item"><a href="#" title="Теги">Теги</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 copyright">
                    <p>Copyright © 2016-<?= date('Y') ?> «For ENERGY» <br>Копирование материалов сайта запрещено!</p>
                </div>
                <div class="col-md-6 text-right">
                    Counters
                </div>
            </div>
        </div>
    </div>

</footer>


<?= LoginWidget::widget() ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
