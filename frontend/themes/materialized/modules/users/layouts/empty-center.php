<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\core\extensions\FlashMsg\FlashMsg;
use emilasp\site\common\assets\ThemeBaseAsset;
use yii\helpers\Html;
use emilasp\admintheme\bundles\ThemeAsset;
use frontend\assets\AppAsset;

?>

<?php ThemeBaseAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php FlashMsg::widget(); ?>

<div id="dashboard">

    <div id="wrapper">

        <div id="content">

            <div class="menubar">
                <div class="sidebar-toggler visible-xs">
                    <i class="ion-navicon"></i>
                </div>
            </div>

            <div class="content-wrapper">

                <?= $content ?>

            </div>
        </div>
    </div>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
