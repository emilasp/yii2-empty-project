<?php

use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\frontend\components\CategoryMenuComponent;


/*$categoryPsychology = ContentCategory::findOne(['code' => 'psixologiya']);
$categoryFastRead   = ContentCategory::findOne(['code' => 'skorochtenie']);
$categoryMember     = ContentCategory::findOne(['code' => 'pamyat']);
$categoryI          = ContentCategory::findOne(['code' => 'poisk-sebya']);*/

return [
/*    [
        'label'  => $categoryPsychology->name,
        'url'    => $categoryPsychology->getUrl('/cms/article/category'),
        'active' => false,
    ],
    [
        'label'  => $categoryI->name,
        'url'    => $categoryI->getUrl('/cms/article/category'),
        'active' => false,
    ],
    [
        'label'  => $categoryFastRead->name,
        'url'    => $categoryFastRead->getUrl('/cms/article/category'),
        'active' => false,
    ],
    [
        'label'  => $categoryMember->name,
        'url'    => $categoryMember->getUrl('/cms/article/category'),
        'active' => false,
    ],*/

    [
        'label'  => Yii::t('cms', 'Articles'),
        'url'    => '/cms/article/index',
        'active' => false,
    ],
    [
        'label'  => Yii::t('cms', 'News'),
        'url'    => '/cms/news/index',
        'active' => false,
    ],
    [
        'label'  => Yii::t('taxonomy', 'Tags'),
        'url'    => '/cms/article/tags',
        'active' => false,
    ],
];
