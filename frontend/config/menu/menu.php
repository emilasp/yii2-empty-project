<?php

use emilasp\cms\common\models\ContentCategory;
use yii\helpers\Url;

$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [

    [
        'label' => Yii::t('users', 'Users'),
        'url'   => '#',
        'icon'  => 'fa fa-users',
        'items' => [
            [
                'label' => Yii::t('users', 'Management'),
                'url'   => '/users/manage/index',
                'icon'  => 'fa fa-user',
                'role'  => ['admin', 'users/manage/index'],
            ],
            [
                'label' => Yii::t('users', 'Issues'),
                'url'   => '/issue/user-issue/index',
                'icon'  => 'fa fa-exclamation-triangle',
                'role'  => ['admin', 'users/user/index'],
            ],
        ],
    ],

    [
        'label' => Yii::t('rights', 'Rights'),
        'url'   => '#',
        'icon'  => 'fa fa-lock',
        'items' => [
            [
                'label' => Yii::t('rights', 'Groups'),
                'url'   => '/rights/rights-group/index',
                'icon'  => 'fa fa-check-square',
                'role'  => ['admin', 'rights/rights-group/index'],
            ],
            [
                'label' => Yii::t('rights', 'Actions'),
                'url'   => '/rights/rights-action/index',
                'icon'  => 'fa fa-anchor',
                'role'  => ['admin', 'rights/rights-action/index'],
            ],
            [
                'label' => Yii::t('rights', 'Parse Actions'),
                'url'   => '/rights/parse-actions/index',
                'icon'  => 'fa fa-bars',
                'role'  => ['admin', 'rights/parse-actions/index'],
            ],
        ],
    ],


    [
        'label' => Yii::t('settings', 'Settings'),
        'url'   => '/settings/default/index',
        'icon'  => 'fa fa-cogs',
        'role'  => ['admin'],
        //'visible' => Yii::$app->user->can('admin')
    ],

    [
        'label' => 'SYSTEM',
        'url'   => '#',
        'icon'  => 'fa fa-users',
        'items' => [
            [
                'label' => 'Logger Logs',
                'url'   => '/system/logger/index',
                'icon'  => 'fa fa-ellipsis-h',
                'role'  => ['admin'],
            ],
            [
                'label' => 'Правка начальных балансов',
                'url'   => '/system/accounting/update-balance-start',
                'icon'  => 'fa fa-ellipsis-h',
                'role'  => ['admin'],
            ],


        ],
    ],


    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '/users/auth/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label'       => Yii::t('users', 'Logout') . ' <small>(' . $username . ')</small>',
        'url'         => '/users/user/logout',
        'icon'        => 'fa fa-key',
        'linkOptions' => ['data-method' => 'POST'],
        'role'        => ['@'],
        'visible'     => !Yii::$app->user->isGuest
    ],
];
