<?php

use emilasp\media\models\File;
use yii\helpers\Html;

$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

$file = Yii::$app->user->identity->profile->image ??new File(['type' => File::TYPE_FILE_IMAGE]);

$avatar = Html::img($file->getUrl(File::SIZE_ICO), ['class' => 'avatar']);
$avatar .= $username . Html::tag('i', null, ['class' => 'fa fa-chevron-down']);

return [
    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '#',
        'options'     => ['class' => 'nav-link auth-form'],
        //'url'     => '/users/service/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest,
        'active'  => false,
    ],

    [
        'label'  => $avatar,
        'url'     => '#',
        'role'   => ['@'],
        'active' => false,
        'items' => [
            [
                'label'       => Yii::t('users', 'Profile'),
                'url'         => '/users/profile/my',
                'icon'        => 'fa fa-user-circle',
                'role'        => ['@'],
                'active'      => false,
            ],
            [
                'label'       => Yii::t('users', 'Logout'),
                'url'         => '/users/service/logout',
                'icon'        => 'fa fa-sign-out',
                'linkOptions' => ['data-method' => 'POST'],
                'role'        => ['@'],
                'active'      => false,
            ],
        ]
    ],
];
