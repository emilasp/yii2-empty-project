<?php

use yii\web\View;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'emilasp\seo\components\SeoBootstrap'],
    'controllerNamespace' => 'emilasp\site\controllers',
    // 'defaultRoute'        => '/tasks/dashboard',
    'language'            => 'ru_RU',
    'sourceLanguage'      => 'en_US',

    'modules' => [
        'core' => [
            'class'             => 'emilasp\core\CoreModule',
            'enableTheme'       => true,
            'enableModuleTheme' => true,
            'theme'             => 'materialized',
        ],

        'taxonomy' => [
            'class' => 'emilasp\taxonomy\TaxonomyModule',
        ],

        'site' => [
            'class' => 'emilasp\site\frontend\SiteModule',
        ],

        'cms'    => [
            'class' => 'emilasp\cms\frontend\CmsModule',
        ],
        'social' => [
            'class' => 'emilasp\social\frontend\SocialModule',
        ],

        'users' => [
            'class'           => 'emilasp\users\frontend\UsersModule',
            'routeAfterLogin' => '/'
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],

    'components' => [
        'request'              => [
            'enableCookieValidation' => true,
            'enableCsrfValidation'   => true,
            'cookieValidationKey'    => 'werwer56yhujm,86756weavmf5017',
        ],
        'user'                 => [
            'class'           => 'emilasp\users\common\components\UserComponent',
            'identityClass'   => 'emilasp\users\common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl'        => ['/users/user/login']
        ],
        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'google'    => [
                    'class'        => 'yii\authclient\clients\Google',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'yandex'    => [
                    'class'        => 'yii\authclient\clients\Yandex',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'facebook'  => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'vkontakte' => [
                    'class'        => 'yii\authclient\clients\VKontakte',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
            ],
        ],
        'access'               => [
            'class' => 'yii\web\AccessControl',
        ],
        'session'              => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],

        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['seo'],
                ],
                [
                    'class' => 'yii\log\DbTarget',
                    'categories'     => ['seo'],
                    'levels'         => ['error', 'warning'],
                    'exportInterval' => 1,
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'categories'     => ['seo'],
                    'levels'         => ['error', 'warning'],
                    'logFile'        => '@runtime/logs/seo/seoData-not-found.log',
                    'exportInterval' => 1,
                    'logVars'        => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/site/site/error',
        ],


        'assetManager' => [
            'linkAssets' => YII_DEBUG,
        ],
        'view'         => [
            'class'           => 'emilasp\core\components\base\View',
            'enableMinify'    => false,
            'webPath'        => '@web',
            'basePath'       => '@webroot',
            'minifyPath'     => '@webroot/minify',
            'jsPosition'     => [View::POS_HEAD, View::POS_READY, View::POS_END],
            'forceCharset'   => 'UTF-8',
            'expandImports'  => true,
            'minifyOutput' => true,
            'theme'           => [
                'pathMap' => [
                    '@app/views' => '@app/themes/materialized/views',
                ],
            ],
        ],

        'urlManager' => [
            'rules' => [
                '' => '/site/site/index',
            ],
        ]
    ],

    /* 'as beforeRequest' => [
         'class' => 'emilasp\seo\behaviors\SeoMetaFilter',
     ],*/

    'params' => $params,
];
