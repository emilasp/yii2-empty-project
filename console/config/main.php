<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],

    'controllerMap' => [
        'migrate' => [
            'class'   => 'emilasp\core\commands\MigrateController',
            'default' => [
                'db'     => 'db',
                'module' => 'app',
            ],
            'modules' => [
                'users'    => '@vendor/emilasp/yii2-user-x/migrations',
                'rights'   => '@vendor/emilasp/yii2-rights/migrations',
                'variety'  => '@vendor/emilasp/yii2-variety/migrations',
                'settings' => '@vendor/emilasp/yii2-settings/migrations',
                'json'     => '@vendor/emilasp/yii2-json/migrations',
                'issue'    => '@vendor/emilasp/yii2-user-issue/migrations',
                'media'    => '@vendor/emilasp/yii2-media/migrations',
                'taxonomy' => '@vendor/emilasp/yii2-taxonomy-x/migrations',
                'seo'      => '@vendor/emilasp/yii2-seo-x/migrations',
                'site'     => '@vendor/emilasp/yii2-site-x/migrations',
                'social'   => '@vendor/emilasp/yii2-social/migrations',
                'cms'      => '@vendor/emilasp/yii2-cms/migrations',
                'course'   => '@vendor/emilasp/yii2-course/migrations',
            ],
        ],
    ],

    'params' => $params,
];
